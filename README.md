# Информация
- Разработчик Дёмин Роман Дмитриевич (LilliumRU)
- Сайт Lillium.ru
- Сайт дизайна ui.Lillium.RU
- Дизайн LilliumRU UI 2.0 Breeze
- CMS - DataLife Engine 14.1 - https://dle-news.ru/

# Credits
- php
- html
- class
- JS
- QT5
- GitHUB - https://github.com/
- KDE - https://kde.org/
- Kirigami - https://github.com/KDE/kirigami
- KDE Human Interface Guidelines - https://hig.kde.org/index.html
