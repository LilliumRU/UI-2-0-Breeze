<div id="footer-cont">   
<table id="footer-table">
<tbody>
<tr>
    
<td id="footer-td1">
<b>© 2008-2021 Проект</b> <a title="LilliumRU" href="http://Lillium.RU/">Lillium.RU</a><br>
<b>Система Управление:</b> <a title="DataLife Engine" href="http://dle-news.ru/">DataLife Engine</a> 14.1 
</td>
    
<td id="footer-td2">
<a class="button-footer" title="rss" href="/rss.xml"><img src="http://files.lillium.ru/image/social/rss.png" width="30px"></a>
<a class="button-footer" title="GitLAB" href="https://gitlab.com/LilliumRU"><img src="http://download.lillium.ru/site/gitlabcom/gitlab.png" width="30px"></a>     
<a class="button-footer" title="GitHUB" href="https://github.com/LilliumRU"><img src="http://files.lillium.ru/image/social/github.png" width="30px"></a>    
<a class="button-footer" title="KDE" href="https://invent.kde.org/lilliumru"><img src="https://invent.kde.org/uploads/-/system/appearance/header_logo/1/kde-logo-white-blue-128x128.png" width="30px"></a>      
<a class="button-footer" title="Yandex Дзен" href="https://zen.yandex.ru/id/5e2c425fc31e4900b03bd0d4"><img src="http://download.lillium.ru/site/zenyandex.ru/zen.png" width="30px"></a>         
<a class="button-footer" title="Yandex Мессенжер" href="https://yandex.ru/chat/#/join/77cde5b5-db80-457c-a2c2-f555dafedc36"><img src="http://files.lillium.ru/image/social/yandexmes.png" width="30px"></a>     
<a class="button-footer" title="telegram" href="https://t.me/lilliumru"><img src="http://files.lillium.ru/image/social/telegram.png" width="30px"></a> 
<a class="button-footer" title="vk" href="https://vk.com/lilliumru_site"><img src="http://files.lillium.ru/image/social/vk.png" width="30px"></a>
<a class="button-footer" title="ок" href="https://ok.ru/group/55748209213501"><img src="http://files.lillium.ru/image/social/ok.png" width="30px"></a>   
<a class="button-footer" title="discord" href="https://discord.gg/E7RGuEY"><img src="http://files.lillium.ru/image/social/discord.png" width="30px"></a>       
<a class="button-footer" title="patreon" href="https://www.patreon.com/lilliumru"><img src="http://files.lillium.ru/image/social/patrion.png" width="30px"></a>   
<a class="button-footer" title="Steam" href="https://steamcommunity.com/groups/lilliumruguild"><img src="http://files.lillium.ru/image/social/steam.png" width="30px"></a>         
</td>
    
<td id="footer-td3">
<b>Дизайн:</b> <a title="Дизайн LilliumRU UI" href="http://ui.Lillium.RU/">LilliumRU UI 2.0 KDE Breeze</a><br>
<b>Версия сайта ver. 2.02.20200926.5.19.14.1</b>  
</td>
    
</tr>
</tbody>
</table>
</div>  
